#!/usr/bin/env python3

from flask import Flask, render_template, request
import database
from urllib.parse import unquote
from subprocess import getoutput
import string
import json
import interactions
import os

HOME = os.environ.get("HOME")
HERE = os.path.dirname(os.path.abspath(__file__))

app = Flask(__name__)

@app.route('/')
def mainpage():
    conn, cursor = database.init_connection("drugs.sqlite")
    drugs_all = []
    for char in string.ascii_lowercase:
        drugs = database.get_drugs_startswith(cursor, "{}%".format(char))
        drugs_all.extend(drugs)
    drugs_all = '["{}"];'.format('", "'.join(drugs_all))
    commit_version = getoutput('/usr/bin/git rev-parse HEAD')[:8]

    return render_template("main.html", drugs_all=drugs_all,
            commit_version=commit_version)

@app.route("/submit", methods=["POST"])
def get_interactions():
    form = request.form['data']
    drugs_list = form.split('&')
    drug_names = [drug.split('=')[1] for drug in drugs_list]
    # unescape html characters
    drug_names = [unquote(drug) for drug in drug_names]
    print(drug_names)

    warnings = interactions.find_interactions(drug_names)
    warnings = interactions.to_string(warnings)
    print(warnings)
    return warnings

if __name__ == "__main__":
    app.run(host='0.0.0.0')
