#!/usr/bin/env python3

import requests
from bs4 import BeautifulSoup as bs

def search_google(drug):
    query = 'https://www.google.com/search?q={drug}+medscape'.format(
            drug=drug)
    req = requests.get(query)
    soup = bs(req.text, "html.parser")
    found = False
    for link in soup.find_all('a'):
        hyperlink = link.get("href")
        if hyperlink:
            direct_url = hyperlink[7:]
            if direct_url.startswith(
                    ("https://reference.medscape.com",
                     "http://reference.medscape.com")):
                found = True
                print("{drug:<20} -> {url}".format(
                    drug=drug,
                    url=direct_url.split('&')[0]))
                break
    if not found:
        print("NOT FOUND: {}".format(drug))

def main():
    drugs_list = []
    with open('drugslist.txt', 'r') as f:
        for line in f:
            drugs_list.append(line.strip())

    for drug in drugs_list:
        search_google(drug)

if __name__ == '__main__':
    main()
