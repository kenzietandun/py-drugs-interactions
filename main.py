#!/usr/bin/env python3

import interactions
import database
import sys
import os
from termcolor import cprint

def print_banner(drugs=[], clear_screen=True):
    """print banner"""
    if clear_screen:
        os.system('clear')
    print("""Currently there's {total} drugs in the list
{drugs}
1. Add drugs
2. Delete drugs
3. Find interactions
9. Exit""".format(total=len(drugs), drugs=drugs))

def get_drug_choice(drugs):
    drugs = sorted(drugs)
    for i, drug in enumerate(drugs):
        print("{index:>2}. {drug}".format(
            index=i,
            drug=drug))

    print()
    choice = int(input("Drug number? "))
    return drugs[choice]

def get_drugs_input():
    """get drugs input from user"""
    conn, cursor = database.init_connection('drugs.sqlite')
    drug_list = []
    print_banner()
    while True:
        print()
        clear_screen = True
        user_input = input('Menu > ')
        while not user_input.isdigit():
            print("Beep boop, I don't understand '{}'".format(user_input))
            user_input = input('Menu > ')

        user_input = int(user_input)
        if user_input == 1:
            keyword = input('Drug starts with> ')
            keyword += '%'
            drugs_with_keyword = database.get_drugs_startswith(cursor, keyword)
            if drugs_with_keyword:
                drug_to_add = get_drug_choice(drugs_with_keyword)
                drug_list.append(drug_to_add)
                cprint("Added {}".format(drug_to_add), "green")
            else:
                cprint("[ERR] No drugs found", "red")
                pass
            print()
        elif user_input == 3: # find interactions
            warnings = interactions.find_interactions(drug_list)
            for level, combinations in warnings.items():
                if level == "contraindicated":
                    color = "red"
                elif level == "serious":
                    color = "yellow"
                elif level == "monitor_closely":
                    color = "blue"
                elif level == "minor":
                    color = "green"
                if combinations:
                    cprint("Level: {}".format(level.upper()), color)
                    for target, incompabilities in combinations:
                        cprint("{t} is not compatible with {i}".format(
                            t=target, i=incompabilities), color)
                else:
                    cprint("Level: {} > No incompabilities found, all good.".format(level), 
                            "green")
            print()
            clear_screen = False

        elif user_input == 2: # delete drugs
            if len(drug_list) == 1:
                cprint("Removed {}".format(drug_list[0]), "yellow")
                drug_list = []
            elif len(drug_list) > 0:
                drug_to_remove = get_drug_choice(drug_list)
                if drug_to_remove in drug_list:
                    drug_list.remove(drug_to_remove)
                    cprint("Removed {}".format(drug_to_remove), "yellow")
                else:
                    cprint("{} is not in the list".format(drug_to_remove), "red")
            else:
                cprint("[ERR] There's no drugs in the list", "red")
            print()
        elif user_input == 9: # exit
            sys.exit(0)
        else:
            pass

        print_banner(drugs=drug_list, clear_screen=clear_screen)

def main():
    get_drugs_input()

if __name__ == '__main__':
    main()
