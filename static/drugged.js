	$( function() {
		var selectedDrugs = [];		
		//autocomplete by first letter
		$('#sear').autocomplete({
			source: function( request, response ) {
				var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( request.term ), "i" );
				response( $.grep( drugs_all, function( item ){
					return matcher.test( item );
				}) );
			}
		});
		
		//add searchbox value to table
		$('#addRow').click(function(e){
			if($.inArray($('#sear').val(), drugs_all) > -1 && $.inArray($('#sear').val(), selectedDrugs) < 0){
				$('#pickedTable tbody').append('<tr class="child"><td><input name="drug" type="text" class="form-control" value="'+document.getElementById('sear').value+'" readonly="readonly" /></td><td align="center"><button class="remove btn btn-danger" id="delRow">remove</button></td></tr>');
				selectedDrugs.push($('#sear').val());
			} else if ($('#sear').val() == ""){
				$('.errormessage').empty();
				$('.errormessage').append($('#sear').val()+"Box is empty!");
			} else if ($.inArray($('#sear').val(), drugs_all) == -1){
				$('.errormessage').empty();
				$('.errormessage').append($('#sear').val()+" is not in the list!");
			} else { 
				$('.errormessage').empty();
				e.preventDefault()
				}
		});
		
//		//display all drugs
//		drugs_all.forEach(function(drugs){
//			$('#listTable tbody').append('<tr><td id="drugname">'+drugs+'</td><td align="center"><button class="btn btn-primary" id="addRow1">Add</button></td></tr>');
//		});
		
		//filter displayed drugs by first letter
		$('.filterarray').click(function(e){
			var filteredDrugs = [];
			$('#listTable tr').remove();
			$.each(drugs_all, function(index, item) {
				if ( item.indexOf($(e.target).text().toLowerCase()) === 0 ) {
					filteredDrugs.push(item);
					$('#listTable tbody').append('<tr><td id="drugname">'+item+'</td><td align="center"><button class="btn btn-primary" id="addRow1">Add</button></td></tr>');
				}
			});
		});
		
		//add drugs from display table
		$(document).on('click', '#addRow1', function(){
			$('.errormessage').empty();
			if($.inArray($(this).closest('td').prev('td').text(), selectedDrugs) < 0 ) {
				$('#pickedTable tbody').append('<tr class="child"><td class="pickedDrug"><input name="drug" type="text" class="form-control" value="'+ $(this).closest('td').prev('td').text() +'" readonly="readonly" /></td><td align="center"><button class="btn btn-danger remove"class="btn btn-primary" id="delRow">remove</button></td></tr>');
				selectedDrugs.push($(this).closest('td').prev('td').text());
			}
		});
		
		//remove drug from picked table
		$('table').on('click','tr button.remove',function(e){
			$('.errormessage').empty();
			//e.preventDefault();
			$(this).closest('tr').remove();
			selectedDrugs.splice($.inArray($(this).closest('tr').find("input").val(), selectedDrugs), 1);
		});
				
		//post function
		$('button#interaction').click(function(e){
            e.preventDefault();
            $.post("/submit", {data: $("#chosenDrugs").serialize()}, function(data) {
                alert(data);
            });
        });
		
		//reset table
		$('button#resetTable').click(function(e){
			e.preventDefault();
			$('#pickedTable>tbody tr').remove();
			selectedDrugs = [];
		});
		
		//add drug in textbox via enter button
		$(document).on('keypress', '#sear', function(e) {
			if (e.which == 13) {
				if($.inArray($('#sear').val(), drugs_all) > -1 && $.inArray($('#sear').val(), selectedDrugs) < 0){
					$('#pickedTable tbody').append('<tr class="child"><td><input type="text" class="form-control" value="'+document.getElementById('sear').value+'" readonly="readonly" /></td><td align="center"><button class="remove btn btn-danger" id="delRow">remove</button></td></tr>');
					selectedDrugs.push($('#sear').val());
				} else if ($('#sear').val() == ""){
					$('.errormessage').empty();
					$('.errormessage').append($('#sear').val()+"Box is empty!");
				} else if ($.inArray($('#sear').val(), drugs_all) == -1){
					$('.errormessage').empty();
					$('.errormessage').append($('#sear').val()+" is not in the list!");
				} else { 
					$('.errormessage').empty();
					//e.preventDefault()
				} 
				e.preventDefault();
			}
		});
		/* some irrelevant functions
		$('#interaction').click(function(){
			var selectedDrugs = [];
			$('#pickedTable tbody tr').each(function(){
				var drugData = $(this).find('td.pickedDrug').text();
				selectedDrugs.push(drugData);
			});
			
			if(selectedDrugs.length === 0){
				alert("Please choose your poison");
			}
				else { 
					//alert(selectedDrugs); 
					$.post( "test.php", function(selectedDrugs) {
						alert( "Data Loaded: ");
					});
				}
		});
		*/
	});
