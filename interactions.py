#!/usr/bin/env python3

import database

def find_interactions(drug_list):
    if not drug_list:
        return

    interaction_dict = {}
    conn, cursor = database.init_connection('drugs.sqlite')
    for level in ['contraindicated', 'serious', 'monitor_closely', 'minor']:
        interaction_dict[level] = []
        for drug in drug_list:
            interactions = database.get_drug_interactions(cursor, level, drug)
            warnings = list(set(drug_list).intersection(set(interactions)))
            if warnings:
                interaction_dict[level].append((drug, warnings))
    return interaction_dict

def to_string(interaction_dict):
    """string representation of interaction_dict"""
    string = ''
    for level in ['contraindicated', 'serious', 'monitor_closely', 'minor']:
        string += "Level: {}\n".format(level.upper())
        for drug, warning in interaction_dict[level]:
            string += "- {drug} is incompatible with {warning}\n".format(
                    drug=drug,
                    warning=warning)
        if not interaction_dict[level]:
            string += "- None\n"
    return string
