#!/usr/bin/env python3

from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from bs4 import BeautifulSoup as bs
import database
import google
import time

def parse_drug_url(driver, medscape_url):
    drug_dict = {}

    driver.get(medscape_url)
    time.sleep(3)
    html_source = driver.page_source
    soup = bs(html_source, "html.parser")
    drug_dict['url'] = medscape_url

    drug_name = soup.find("span", {"class": "drug_section_link"})
    try:
        for tag in drug_name.find_all("span"):
            tag.replaceWith('')
    except AttributeError:
        drug_dict['name'] = ''
        drug_dict['contraindicated'] = ''
        drug_dict['serious'] = ''
        drug_dict['monitor_closely'] = ''
        drug_dict['minor'] = ''
        return drug_dict
    else:
        drug_dict['name'] = drug_name.text

    for drugs in soup.find_all('div', {"id": "druglistcontainer"}):
        drug_dict['contraindicated'] = find_drugs(drugs, "draglist1")
        drug_dict['serious'] = find_drugs(drugs, "draglist2")
        drug_dict['monitor_closely'] = find_drugs(drugs, "draglist3")
        drug_dict['minor'] = find_drugs(drugs, "draglist4")

    return drug_dict

def find_drugs(soup, ul_id):
    drugs = []
    ul_tag = soup.find("ul", {"id": ul_id})
    try:
        for li in ul_tag.find_all("li"):
            for tag in li.find_all("p"):
                tag.replaceWith('')
            drugs.append(li.text)
    except AttributeError:
        return ''

    return ';'.join(drugs)

def main():
    conn, cursor = database.init_connection('drugs.sqlite')

    medscape_interaction_url_section = 3
    drugs_list = []
    with open('logfile', 'r') as f:
        for line in f:
            raw_entry = line.strip()
            raw_entry = ' '.join(raw_entry.split())
            raw_entry = raw_entry.split(' -> ')
            drug_name, drug_url = raw_entry
            drugs_list.append('{url}#{section}'.format(
                url=drug_url,
                section=medscape_interaction_url_section)
                )
    option = Options()
    option.set_headless(headless=True)
    driver = webdriver.Firefox(firefox_options=option)

    for drug_url in drugs_list:
        if drug_url.startswith('http:'):
            drug_url = drug_url.replace('http:', 'https:')
        print(drug_url)
        if database.is_drug_exist(cursor, drug_url):
            print('Skipping {}'.format(drug_url))
        else:
            drug_dict = parse_drug_url(driver, drug_url)
            database.write_to_db(conn, cursor, drug_dict)

if __name__ == '__main__':
    main()
