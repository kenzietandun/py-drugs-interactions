# py-drugs-interactions

Credits to Medscape, all data was scraped from their website.

#### Requirements

- python3

- virtualenv

- pip3/pip

- Linux or macOS, not tested on Windows

#### Installation

- Clone the repository `git clone https://gitlab.com/kenzietandun/py-drugs-interactions.git`

- `cd py-drugs-interactions`

- Create a contained environment for python3 `virtualenv -p python3 ./`

- Install requirements `pip install -r req.txt`

- Activate the environemnt `source bin/activate`

- Run the program `./main.py`

