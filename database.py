#!/usr/bin/env python3

import sqlite3
import os

def init_connection(db_file):
    """initialise connection to sqlite db
    RETURNS sqlite3 conn and cursor tuple"""
    if not os.path.isfile(db_file):
        os.mknod(db_file)

    conn = sqlite3.connect(db_file)
    cursor = conn.cursor()
    cursor.execute("""CREATE TABLE IF NOT EXISTS
        drugs (url TEXT PRIMARY KEY,
               name TEXT, 
               contraindicated TEXT,
               serious TEXT,
               monitor_closely TEXT,
               minor TEXT)""")
    conn.commit()
    return conn, cursor

def get_drugs_startswith(cursor, keyword):
    """get drugs that has name startswith <<keyword>>
    RETURNS list of drugs"""
    cursor.execute("""SELECT name FROM drugs WHERE
        name LIKE (?)""", (keyword,))
    results = cursor.fetchall()
    results = sorted([r[0] for r in results])
    return results

def is_drug_exist(cursor, drug_url):
    """check if drug exists in database
    RETURNS boolean"""
    cursor.execute("""SELECT url FROM drugs
        WHERE url = (?)""", (drug_url,))
    result = cursor.fetchone()
    return result is not None

def get_drug_interactions(cursor, interaction_level, drug_name):
    """get list of drugs that has interaction 
    <<interaction_level>> with <<drug_name>>"""
    cursor.execute("""SELECT {} FROM drugs
        WHERE name = (?)""".format(interaction_level), (drug_name,))
    results = cursor.fetchone()[0]
    if ';' in results:
        results = results.split(';')
    return results

def write_to_db(conn, cursor, drug_dict):
    """write drug data to database"""
    print(drug_dict['name'])
    try:
        cursor.execute("""INSERT OR IGNORE INTO drugs
            VALUES ((?), (?), (?), (?), (?), (?))""",
            (drug_dict['url'], 
             drug_dict['name'], 
             drug_dict['contraindicated'],
             drug_dict['serious'], 
             drug_dict['monitor_closely'],
             drug_dict['minor']))
        conn.commit()
    except KeyError:
        return
